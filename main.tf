terraform {
  required_providers {
    proxmox = {
      source = "Telmate/proxmox"
      version = "2.6.5"
    }
  }
}

variable "node" {
  type = string
  description = "The proxmox node to deploy to"
}

variable "root_user" {
  type = string
  description = "The proxmox root user"
}

variable "passw" {
  type = string
  description = "The proxmox root password"
}

variable "prox_host" {
  type = string
  description = "IP or Host of proxmox"
}

provider "proxmox" {
    pm_api_url = "https://${var.prox_host}:8006/api2/json"
    pm_user = var.root_user
    pm_password = var.passw
    pm_tls_insecure = "true"
}

resource "proxmox_vm_qemu" "proxmox_vm" {
  count             = 3
  agent             = 1
  name              = "tf-vm-${count.index}"
  target_node       = var.node
  clone             = "ubuntu-template"
  full_clone        = false
  bootdisk          = "scsi0"
  vmid              = (600 + count.index)
  lifecycle {
    ignore_changes  = [
      network,
    ]
  }
}